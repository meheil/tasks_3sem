import java.util.*;

public class Fourth {
    // Словарь для преобразования числительных в числа
    private static final Map<String, Integer> NUM_WORDS = new HashMap<>();

    // Инициализация словаря
    static {
        NUM_WORDS.put("zero", 0);
        NUM_WORDS.put("one", 1);
        NUM_WORDS.put("two", 2);
        NUM_WORDS.put("three", 3);
        NUM_WORDS.put("four", 4);
        NUM_WORDS.put("five", 5);
        NUM_WORDS.put("six", 6);
        NUM_WORDS.put("seven", 7);
        NUM_WORDS.put("eight", 8);
        NUM_WORDS.put("nine", 9);
        NUM_WORDS.put("ten", 10);
        NUM_WORDS.put("eleven", 11);
        NUM_WORDS.put("twelve", 12);
        NUM_WORDS.put("thirteen", 13);
        NUM_WORDS.put("fourteen", 14);
        NUM_WORDS.put("fifteen", 15);
        NUM_WORDS.put("sixteen", 16);
        NUM_WORDS.put("seventeen", 17);
        NUM_WORDS.put("eighteen", 18);
        NUM_WORDS.put("nineteen", 19);
        NUM_WORDS.put("twenty", 20);
        NUM_WORDS.put("thirty", 30);
        NUM_WORDS.put("forty", 40);
        NUM_WORDS.put("fifty", 50);
        NUM_WORDS.put("sixty", 60);
        NUM_WORDS.put("seventy", 70);
        NUM_WORDS.put("eighty", 80);
        NUM_WORDS.put("ninety", 90);
        NUM_WORDS.put("hundred", 100);
    }

    public static void main(String[] args) {
        // Примеры вызова функций
        System.out.println(nonRepeatable("abracadabra"));
        System.out.println(nonRepeatable("paparazzi"));
        System.out.println(generateBrackets(1));
        System.out.println(generateBrackets(2));
        System.out.println(generateBrackets(3));
        System.out.println(binarySystem(3));
        System.out.println(binarySystem(4));
        System.out.println(alphabeticRow("abcdjuwx"));
        System.out.println(alphabeticRow("klmabzyxw"));
        System.out.println(countCharacters("aaabbcdd"));
        System.out.println(countCharacters("vvvvaajaaaaa"));
        System.out.println(convertToNum("eight"));
        System.out.println(convertToNum("five hundred sixty seven"));
        System.out.println(convertToNum("thirty one"));
        System.out.println(uniqueSubstring("123412324"));
        System.out.println(uniqueSubstring("111111"));
        System.out.println(uniqueSubstring("77897898"));
        System.out.println(shortestWay(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}})); // ➞ 7
        System.out.println(shortestWay(new int[][]{{2, 7, 3}, {1, 4, 8}, {4, 5, 9}})); // ➞ 21
        System.out.println(numericOrder("t3o the5m 1One all6 r4ule ri2ng")); // ➞ " One ring to rule them all"
        System.out.println(numericOrder("re6sponsibility Wit1h gr5eat power3 4comes g2reat")); // ➞ " With great power comes great responsibility"
        System.out.println(switchNums(519, 723)); // ➞ 953
        System.out.println(switchNums(491, 3912)); // ➞ 9942
        System.out.println(switchNums(6274, 71259)); // ➞ 77659

    }

    // Рекурсивная функция для удаления повторяющихся символов в строке
    public static String nonRepeatable(String s) {
        if (s.length() <= 1) {
            return s;
        }
        // Вызов функции для подстроки без первого символа
        String rest = nonRepeatable(s.substring(1));
        // Если первый символ не повторяется в остатке, добавляем его
        if (rest.indexOf(s.charAt(0)) == -1) {
            return s.charAt(0) + rest;
        } else { // Если повторяется, удаляем все его вхождения из остатка
            return s.charAt(0) + rest.replaceAll(String.valueOf(s.charAt(0)), "");
        }
    }

    // Генерация всех возможных правильных комбинаций пар скобок для заданного числа n
    public static List<String> generateBrackets(int n) {
        List<String> combinations = new ArrayList<>();
        generateCombinations(combinations, "", 0, 0, n);
        return combinations;
    }

    // Вспомогательная рекурсивная функция для генерации комбинаций скобок
    private static void generateCombinations(List<String> combinations, String current, int open, int close, int max) {
        // Если строка достигла нужной длины, добавляем её в список
        if (current.length() == max * 2) {
            combinations.add(current);
            return;
        }

        // Добавляем открывающую скобку, если она еще допустима
        if (open < max) {
            generateCombinations(combinations, current + "(", open + 1, close, max);
        }
        // Добавляем закрывающую скобку, если она допустима (есть открытая скобка)
        if (close < open) {
            generateCombinations(combinations, current + ")", open, close + 1, max);
        }
    }

    // Генерация всех возможных бинарных комбинаций длины n, в которых не может быть соседствующих нулей
    public static List<String> binarySystem(int n) {
        List<String> combinations = new ArrayList<>();
        generateCombinations(combinations, "", n, false);
        return combinations;
    }

    // Вспомогательная рекурсивная функция для генерации бинарных комбинаций
    private static void generateCombinations(List<String> combinations, String current, int remaining, boolean lastZero) {
        // Если достигнута нужная длина, добавляем комбинацию
        if (remaining == 0) {
            combinations.add(current);
            return;
        }

        // Добавляем 0, если предыдущая цифра не была 0
        if (!lastZero) {
            generateCombinations(combinations, current + "0", remaining - 1, true);
        }
        // Добавляем 1
        generateCombinations(combinations, current + "1", remaining - 1, false);
    }

    // Поиск самой длинной последовательности букв в алфавитном порядке
    public static String alphabeticRow(String s) {
        String longestRow = "";
        String currentRow = String.valueOf(s.charAt(0));

        for (int i = 1; i < s.length(); i++) {
            // Если следующая буква идет после предыдущей в алфавитном порядке, добавляем её в текущую последовательность
            if (Math.abs(s.charAt(i) - s.charAt(i - 1)) == 1) {
                currentRow += s.charAt(i);
            } else { // Иначе сравниваем текущую последовательность с самой длинной
                if (currentRow.length() > longestRow.length()) {
                    longestRow = currentRow;
                }
                currentRow = String.valueOf(s.charAt(i));
            }
        }

        // Проверка, если последовательность в конце строки
        if (currentRow.length() > longestRow.length()) {
            longestRow = currentRow;
        }

        return longestRow;
    }

    // Подсчет повторяющихся символов в строке и сортировка по длине буквенного паттерна
    public static String countCharacters(String s) {
        List<String> patterns = new ArrayList<>();
        int count = 1;
        for (int i = 1; i <= s.length(); i++) {
            // Если следующий символ не равен предыдущему, добавляем букву и количество повторов в список
            if (i == s.length() || s.charAt(i) != s.charAt(i - 1)) {
                patterns.add(s.charAt(i - 1) + String.valueOf(count));
                count = 1;
            } else { // Иначе увеличиваем количество повторов
                count++;
            }
        }
        // Сортировка списка по длине буквенного паттерна
        patterns.sort(Comparator.comparingInt(a -> a.length()));
        // Объединение элементов списка в одну строку
        return String.join("", patterns);
    }

    // Преобразование строки, содержащей числа внутри слов, в числа
    public static int convertToNum(String str) {
        int result = 0;
        int currentNumber = 0;
        String[] words = str.split(" ");
        // Перебор слов в строке
        for (String word : words) {
            if (NUM_WORDS.containsKey(word)) {
                int num = NUM_WORDS.get(word);
                // Обработка случая с числительным "hundred"
                if (num == 100) {
                    if (currentNumber == 0) {
                        currentNumber = 1;
                    }
                    currentNumber *= num;
                } else if (num >= 10) { // Добавление числа к текущему числу
                    currentNumber += num;
                } else { // Обработка чисел от 1 до 9
                    currentNumber += num;
                }
            }
        }
        result += currentNumber;
        return result;
    }

    // Поиск подстроки максимальной длины с уникальными элементами
    public static String uniqueSubstring(String s) {
        String longestSubstring = "";
        // Перебор всех символов в строке
        for (int i = 0; i < s.length(); i++) {
            Set<Character> uniqueChars = new HashSet<>();
            int j = i;
            // Поиск подстроки с уникальными символами, начиная с текущего символа
            while (j < s.length() && !uniqueChars.contains(s.charAt(j))) {
                uniqueChars.add(s.charAt(j));
                j++;
            }
            // Проверка длины найденной подстроки и обновление самой длинной подстроки при необходимости
            if (j - i > longestSubstring.length()) {
                longestSubstring = s.substring(i, j);
            }
        }
        return longestSubstring;
    }

    // Поиск кратчайшего пути с минимальной суммой чисел, передвигаясь только вправо или вниз
    public static int shortestWay(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        // Обход матрицы и нахождение минимального пути
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) continue;
                else if (i == 0) grid[i][j] += grid[i][j - 1];
                else if (j == 0) grid[i][j] += grid[i - 1][j];
                else grid[i][j] += Math.min(grid[i - 1][j], grid[i][j - 1]);
            }
        }
        // Возвращаем сумму чисел в правом нижнем углу, которая представляет собой минимальную сумму пути
        return grid[m - 1][n - 1];
    }

    // Переупорядочивание слов в строке по числам, встречающимся в словах
    public static String numericOrder(String s) {
        String[] words = s.split(" ");
        String[] orderedWords = new String[words.length];
        // Обход слов и размещение их в порядке, заданном числами, встречающимися в словах
        for (String word : words) {
            for (char c : word.toCharArray()) {
                if (Character.isDigit(c)) {
                    orderedWords[Character.getNumericValue(c) - 1] = word.replaceAll("\\d", "");
                }
            }
        }
        // Объединение слов в строку
        return String.join(" ", orderedWords);
    }

    // Замена цифр одного числа на другое максимально возможное число, беря цифры только из первого числа
    public static int switchNums(int num1, int num2) {
        char[] arr1 = String.valueOf(num1).toCharArray();
        char[] arr2 = String.valueOf(num2).toCharArray();
        // Сортировка цифр первого числа по убыванию
        Arrays.sort(arr1);

        // Замена цифр второго числа на максимально возможные из первого числа
        for (int i = 0; i < arr2.length; i++) {
            for (int j = arr1.length - 1; j >= 0; j--) {
                if (arr1[j] > arr2[i]) {
                    arr2[i] = arr1[j];
                    arr1[j] = '0';
                    break;
                }
            }
        }
        // Преобразование массива символов обратно в число и его возврат
        return Integer.parseInt(new String(arr2));
    }
}
