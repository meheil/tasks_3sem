public class Fifth {

    public static int countDuplicates(String input) {
        // Создаем массив для подсчета количества каждого символа.
        // Для всех символов типа char достаточно 65536 мест.
        int[] charCounts = new int[Character.MAX_VALUE + 1];
        int duplicateCount = 0;

        // Считаем, сколько раз каждый символ встречается в строке
        for (char ch : input.toCharArray()) {
            charCounts[ch]++;
        }

        // Подсчитываем количество дубликатов
        for (int count : charCounts) {
            if (count > 1) {
                duplicateCount += count - 1; // Если символ встречается более одного раза, добавляем к общему количеству дубликатов
            }
        }

        return duplicateCount;
    }

    public static void main(String[] args) {
        String testString = "Hello World";
        System.out.println("duplicates(\"" + testString + "\") -> " + countDuplicates(testString));
    }
}
