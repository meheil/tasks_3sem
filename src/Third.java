import java.util.Arrays;
public class Third {
    public static void main(String[] args) {
        // 1. Замена всех гласных букв в строке на символ '*'
        System.out.println(replaceVowels("apple") + "\n" + replaceVowels("Even if you did this task not by yourself, you have to understand every single line of code.") + "\n");

        // 2. Замена двух идущих подряд букв по шаблону "Double*"
        System.out.println(stringTransform("hello") + "\n" + stringTransform("bookkeeper") + "\n");

        // 3. Проверка, поместится ли блок в отверстие с заданными параметрами
        System.out.println(doesBlockFit(1, 3, 5, 4, 5) + "\n" + doesBlockFit(1, 8, 1, 1, 1) + "\n" + doesBlockFit(1, 2, 2, 1, 1) + "\n");

        // 4. Проверка четности суммы квадратов цифр числа
        System.out.println(numCheck(243) + "\n" + numCheck(52) + "\n");

        // 5. Подсчет количества целочисленных корней квадратного уравнения
        System.out.println(countRoots(new int[]{1, -3, 2}) + "\n" + countRoots(new int[]{2, 5, 2}) + "\n" + countRoots(new int[]{1, -6, 9}) + "\n");

        // 6. Поиск товаров, проданных в каждом магазине
        System.out.println(Arrays.toString(salesData(new String[][]{{"Apple", "Shop1", "Shop2", "Shop3", "Shop4"}, {"Banana", "Shop2", "Shop3", "Shop4"}, {"Orange", "Shop1", "Shop3", "Shop4"}, {"Pear", "Shop2", "Shop4"}})));
        System.out.println(Arrays.toString(salesData(new String[][]{{"Fridge", "Shop2", "Shop3"}, {"Microwave", "Shop1", "Shop2", "Shop3", "Shop4"}, {"Laptop", "Shop3", "Shop4"}, {"Phone", "Shop1", "Shop2", "Shop3", "Shop4"}})) + "\n");

        // 7. Проверка возможности разбиения строки на слова, начинающиеся с последней буквы предыдущего слова
        System.out.println(validSplit("apple eagle egg goat") + "\n" + validSplit("cat dog goose fish") + "\n");

        // 8. Проверка, является ли массив "волнообразным"
        System.out.println(waveForm(new int[]{3, 1, 4, 2, 7, 5}) + "\n" + waveForm(new int[]{1, 2, 3, 4, 5}) + "\n" + waveForm(new int[]{1, 2, -6, 10, 3}) + "\n");

        // 9. Поиск наиболее часто встречающейся гласной в предложении
        System.out.println(commonVowel("Hello world") + "\n" + commonVowel("Actions speak louder than words.") + "\n");

        // 10. Изменение каждого n-го элемента n-го массива на среднее арифметическое элементов n-го столбца остальных массивов
        System.out.println(Arrays.deepToString(dataScience(new int[][]{{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {5, 5, 5, 5, 5}, {7, 4, 3, 14, 2}, {1, 0, 11, 10, 1}})));
    }

    // 1. Замена всех гласных букв в строке на символ '*'
    public static String replaceVowels(String str) {
        // Определение гласных букв
        String vowels = "aeiouyAEIOUY";
        String temp;
        for (int i = 0; i < str.length(); i++) {
            temp = "";
            temp += str.charAt(i);
            // Замена гласных букв на '*'
            if (vowels.contains(temp)) str = str.replace(temp, "*");
        }
        return str;
    }

    // 2. Замена двух идущих подряд букв по шаблону "Double*"
    public static String stringTransform(String str) {
        String temp;
        temp = "";
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == str.charAt(i + 1)) {
                // Если две буквы подряд, то заменяем их по шаблону "Double*"
                temp += "Double" + String.valueOf(str.charAt(i + 1)).toUpperCase();
                i++;
            } else temp += str.charAt(i);
        }
        temp += str.charAt(str.length() - 1);
        return temp;
    }

    // 3. Проверка, поместится ли блок в отверстие с заданными параметрами
    public static boolean doesBlockFit(int x, int y, int z, int w, int h) {
        int[] block_size = new int[3];
        block_size[0] = x;
        block_size[1] = y;
        block_size[2] = z;
        Arrays.sort(block_size);
        // Проверка соответствия размеров блока и отверстия
        if ((block_size[0] <= Math.min(w, h)) & (block_size[1] <= Math.max(w, h))) return true;
        return false;
    }

    // 4. Проверка четности суммы квадратов цифр числа
    public static boolean numCheck(int num) {
        int sum = 0;
        int number = num;
        // Вычисление суммы квадратов цифр числа
        while (number > 0) {
            sum += (number % 10) * (number % 10);
            number /= 10;
        }
        // Проверка соответствия четности суммы и числа
        return (sum % 2 == num % 2);
    }

    // 5. Подсчет количества целочисленных корней квадратного уравнения
    public static int countRoots(int[] arr) {
        int a = arr[0];
        int b = arr[1];
        int c = arr[2];
        int d = b * b - 4 * a * c;
        // Определение количества корней квадратного уравнения
        if (d < 0) return 0;
        if (d == 0) {
            double x = (-b) / (2 * a);
            if (x % 1 == 0) return 1;
            return 0;
        }
        int rootsNumber = 0;
        double x1 = (-b + Math.sqrt(d)) / (2 * a);
        if ((x1) % 1 == 0) rootsNumber++;
        double x2 = (-b - Math.sqrt(d)) / (2 * a);
        if ((x2) % 1 == 0) rootsNumber++;
        return rootsNumber;
    }

    // 6. Поиск товаров, проданных в каждом магазине
    public static String[] salesData(String[][] data) {
        String[] temp = new String[data.length];
        int max = 0;
        // Поиск максимального количества продаж
        for (int i = 0; i < data.length; i++) {
            if (data[i].length > max) max = data[i].length;
        }
        int j = 0;
        // Запись товаров, проданных в каждом магазине
        for (int i = 0; i < data.length; i++) {
            if (data[i].length == max) {
                temp[j] = data[i][0];
                j++;
            }
        }
        String[] answer = new String[j];
        for (int i = 0; i < j; i++) answer[i] = temp[i];
        return answer;
    }

    // 7. Проверка возможности разбиения строки на слова, начинающиеся с последней буквы предыдущего слова
    public static boolean validSplit(String str) {
        String[] words = str.split(" ");
        if (words.length < 2) return false;
        // Проверка каждого слова
        for (int i = 1; i < words.length; i++) {
            String word1 = words[i - 1];
            String word2 = words[i];
            if (word1.charAt(word1.length() - 1) != word2.charAt(0)) return false;
        }
        return true;
    }

    // 8. Проверка, является ли массив "волнообразным"
    public static boolean waveForm(int[] arr) {
        boolean flag = true;
        // Проверка четности индексов и значений в массиве
        for (int i = 0; i < arr.length - 1; i += 2) {
            if (arr[i] < arr[i + 1]) flag = false;
        }
        if (flag) return true;
        flag = true;
        for (int i = 1; i < arr.length - 1; i += 2) {
            if (arr[i] < arr[i + 1]) flag = false;
        }
        if (flag) return true;
        return false;
    }

    // 9. Поиск наиболее часто встречающейся гласной в предложении
    public static String commonVowel(String str) {
        str = str.toLowerCase();
        String vowels = "aeiouy";
        int[] count = new int[6];
        // Подсчет количества каждой гласной
        for (char c : str.toCharArray()) {
            if (vowels.indexOf(c) != -1) {
                count[vowels.indexOf(c)]++;
            }
        }
        int max = 0;
        // Нахождение наиболее часто встречающейся гласной
        for (int i = 1; i < count.length; i++) {
            if (count[i] > count[max]) {
                max = i;
            }
        }
        return String.valueOf(vowels.charAt(max));
    }

    // 10. Изменение каждого n-го элемента n-го массива на среднее арифметическое элементов n-го столбца остальных массивов
    public static int[][] dataScience(int[][] arr) {
        int[] avg = new int[arr[0].length];
        // Вычисление среднего арифметического для каждого столбца
        for (int x = 0; x < arr.length; x++) {
            for (int y = 0; y < arr[x].length; y++) {
                if (x != y) {
                    avg[y] += arr[x][y];
                }
            }
        }
        for (int i = 0; i < avg.length; i++) {
            int temp = (int) Math.round((double) avg[i] / (arr[0].length - 1));
            avg[i] = temp;
        }
        // Изменение каждого n-го элемента n-го массива
        for (int x = 0; x < arr.length; x++) {
            for (int y = 0; y < arr[x].length; y++) {
                if (x == y) arr[x][y] = avg[x];
            }
        }
        return arr;
    }
}
