import java.util.Arrays;
import java.util.Random;

public class Second {
    public static void main(String[] args) {
        // Проверка наличия повторяющихся символов в строке
        System.out.println(duplicateChars("Donald") + "\n" + duplicateChars("orange") + "\n");

        // Получение инициалов из имени и фамилии
        System.out.println(getInitials("Ryan Gosling") + "\n" + getInitials("Barack Obama") + "\n");

        // Разница между суммой четных и нечетных чисел в массиве
        System.out.println(differenceEvenOdd(new int[]{44, 32, 86, 19}) + "\n" + differenceEvenOdd(new int[]{22, 50, 16, 63, 31, 55}) + "\n");

        // Проверка наличия элемента, равного среднему арифметическому в массиве
        System.out.println(equalToAvg(new int[]{1, 2, 3, 4, 5}) + "\n" + equalToAvg(new int[]{1, 2, 3, 4, 6}) + "\n");

        // Умножение каждого элемента массива на его индекс
        System.out.println(Arrays.toString(indexMult(new int[]{1, 2, 3})) + "\n" + Arrays.toString(indexMult(new int[]{3, 3, -2, 408, 3, 31})) + "\n");

        // Переворот строки
        System.out.println(reverse("Hello World") + "\n" + reverse("The quick brown fox.") + "\n");

        // Получение числа последовательности Трибоначчи
        System.out.println(tribonacci(7) + "\n" + tribonacci(11) + "\n");

        // Генерация квази-хэша заданной длины
        System.out.println(pseudoHash(5) + "\n" + pseudoHash(10) + "\n" + pseudoHash(0) + "\n");

        // Поиск слова "help" в строке
        System.out.println(botHelper("Hello, I’m under the water, please help me") + "\n" + botHelper("Two pepperoni pizzas please") + "\n");

        // Проверка на анаграммы
        System.out.println(isAnagram("listen", "silent") + "\n" + isAnagram("eleven plus two", "twelve plus one") + "\n" + isAnagram("hello", "world"));
    }

    // Метод для проверки наличия повторяющихся символов в строке
    public static boolean duplicateChars(String str) {
        // Приведение строки к нижнему регистру для учета регистронезависимости
        str = str.toLowerCase();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            // Если количество вхождений символа в строку больше 1, значит символ повторяется
            if ((str.length() - str.replace(String.valueOf(ch), "").length()) > 1) {
                return true;
            }
        }
        return false;
    }

    // Метод для получения инициалов из имени и фамилии
    public static String getInitials(String name) {
        String initials = "";
        for (int i = 0; i < name.length(); i++) {
            // Добавление в инициалы символов, находящихся в верхнем регистре (буквы имени и фамилии)
            if (Character.isUpperCase(name.charAt(i))) initials += name.charAt(i);
        }
        return initials;
    }

    // Метод для вычисления разницы между суммой четных и нечетных чисел в массиве
    public static int differenceEvenOdd(int[] array) {
        int even = 0;
        int odd = 0;
        for (int i = 0; i < array.length; i++) {
            // Суммирование четных и нечетных чисел массива
            if (array[i] % 2 == 0) even += array[i];
            else
                odd += array[i];
        }
        // Возвращение абсолютной разницы между суммой четных и нечетных чисел
        return Math.abs(even - odd);
    }

    // Метод для проверки наличия элемента, равного среднему арифметическому в массиве
    public static boolean equalToAvg(int[] array) {
        int sum = 0;
        // Вычисление суммы всех элементов массива
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        // Вычисление среднего арифметического
        double avg = (double) sum / array.length;
        // Проверка наличия элемента, равного среднему арифметическому
        for (int i = 0; i < array.length; i++) {
            if (array[i] == avg) return true;
        }
        return false;
    }

    // Метод для умножения каждого элемента массива на его индекс
    public static int[] indexMult(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * i;
        }
        return array;
    }

    // Метод для переворота строки
    public static String reverse(String str) {
        // Использование StringBuilder для удобного переворота строки
        StringBuilder s = new StringBuilder(str);
        return s.reverse().toString();
    }

    // Метод для получения числа последовательности Трибоначчи
    public static int tribonacci(int number) {
        int[] arr = new int[number + 4];
        arr[1] = 0;
        arr[2] = 0;
        arr[3] = 1;
        // Вычисление последовательности Трибоначчи
        for (int i = 4; i <= number; i++) {
            arr[i] = arr[i - 1] + arr[i - 2] + arr[i - 3];
        }
        return arr[number];
    }

    // Метод для генерации квази-хэша заданной длины
    public static String pseudoHash(int number) {
        Random random = new Random();
        String alphabet = "0123456789abcdef";
        String hash = "";
        // Генерация хэша
        for (int i = 0; i < number; i++) {
            hash += alphabet.charAt(random.nextInt(16));
        }
        return hash;
    }

    // Метод для поиска слова "help" в строке
    public static String botHelper(String message) {
        // Проверка наличия слова "help" в строке и возврат соответствующего сообщения
        if (message.toLowerCase().contains("help")) return "Calling for a staff member";
        return "Keep waiting";
    }

    // Метод для проверки на анаграммы
    public static boolean isAnagram(String str1, String str2) {
        char[] chars = str1.toCharArray();
        // Сортировка символов в строке для сравнения
        Arrays.sort(chars);
        String srtd1 = new String(chars);
        chars = str2.toCharArray();
        Arrays.sort(chars);
        String srtd2 = new String(chars);
        // Сравнение отсортированных строк для определения анаграммности
        if (srtd1.equals(srtd2)) return true;
        return false;
    }
}
