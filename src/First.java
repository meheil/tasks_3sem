public class First {
    public static void main(String[] args) {
        System.out.println(convert(5) + "\n" + convert(3) + "\n" + convert(8) + "\n");
        System.out.println(fitCalc(15, 1) + "\n" + fitCalc(24, 2) + "\n" + fitCalc(41, 3) + "\n");
        System.out.println(containers(3, 4, 2) + "\n" + containers(5, 0, 2) + "\n" + containers(4, 1, 4) + "\n");
        System.out.println(triangleType(5, 5, 5) + "\n" + triangleType(5, 4, 5) + "\n" + triangleType(3, 4, 5) + "\n");
        System.out.println(ternaryEvaluation(8, 4) + "\n" + ternaryEvaluation(1, 11) + "\n" + ternaryEvaluation(5, 9) + "\n");
        System.out.println(howManyItems(22, 1.4, 2) + "\n" + howManyItems(45, 1.8, 1.9) + "\n" + howManyItems(100, 2, 2) + "\n");
        System.out.println(factorial(3) + "\n" + factorial(5) + "\n" + factorial(7) + "\n");
        System.out.println(gcd(48, 18) + "\n" + gcd(52, 8) + "\n" + gcd(259, 28) + "\n");
        System.out.println(ticketSaler(70, 1500) + "\n" + ticketSaler(24, 950) + "\n" + ticketSaler(53, 1250) + "\n");
        System.out.println(tables(5, 2) + "\n" + tables(31, 20) + "\n" + tables(123, 58) + "\n");
    }

    // Метод для преобразования галлонов в литры
    public static double convert(int gallons) {
        return gallons * 3.785;
    }

    // Метод для вычисления общего количества сожженных калорий
    public static int fitCalc(int minutes, int intensity) {
        return minutes * intensity;
    }

    // Метод для вычисления общего количества товаров
    public static int containers(int boxes, int bags, int barrels) {
        return boxes * 20 + bags * 50 + barrels * 100;
    }

    // Метод для определения типа треугольника
    public static String triangleType(double x, double y, double z) {
        if ((x >= y + z) || (y >= x + z) || (z >= x + y)) return "не является треугольником";
        if ((x == y) && (y == z)) return "равносторонний";
        if ((x == y) || (x == z) || (y == z)) return "равнобедренный";
        return "разносторонний";
    }

    // Метод для оценки тернарных выражений
    public static int ternaryEvaluation(int a, int b) {
        return a > b ? a : b;
    }

    // Метод для определения количества пододеяльников, которые можно сшить
    public static int howManyItems(double n, double w, double h) {
        return (int) (n / 2 / w / h);
    }

    // Метод для вычисления факториала числа
    public static int factorial(int x) {
        if (x <= 0) return 0;
        int result = 1;
        for (int i = 1; i <= x; i++) {
            result *= i;
        }
        return result;
    }

    // Метод для нахождения наибольшего общего делителя двух чисел
    public static int gcd(int a, int b) {
        int result = 1;
        for (int i = 1; i <= Math.min(a, b) / 2; i++) {
            if ((a % i == 0) && (b % i == 0)) result = i;
        }
        return result;
    }

    // Метод для вычисления выручки с продажи билетов
    public static int ticketSaler(int amount, int price) {
        return (int) (amount * price * 0.72);
    }

    // Метод для определения того, скольки парт не хватает в аудитории
    public static int tables(int students, int tables) {
        int result = (students + 1) / 2 - tables;
        if (result < 0) return 0;
        return result;
    }
}